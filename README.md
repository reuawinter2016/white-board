##About This Repo

This is an empty repository, it would not have any source code inside. The purpose of this repo is to share tutorials and reading materials in the Wiki page of this repo.

[Here is the pages](https://bitbucket.org/reuawinter2016/white-board/wiki/Home)

**Everybody** is encouraged to post and share material.

Click [Here](https://bitbucket.org/realestateua/white-board/wiki/Home) to see the Material From Last Term.
